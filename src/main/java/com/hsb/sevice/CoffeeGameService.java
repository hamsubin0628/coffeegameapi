package com.hsb.sevice;

import com.hsb.entity.CoffeeGame;
import com.hsb.entity.Member;
import com.hsb.model.coffeeGame.CoffeeGameItem;
import com.hsb.model.coffeeGame.CoffeeGameCreateRequest;
import com.hsb.model.coffeeGame.CoffeeGameResponse;
import com.hsb.model.coffeeGame.CoffeeGameTotalPriceChangeRequest;
import com.hsb.reposutory.CoffeeGameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CoffeeGameService {
    private final CoffeeGameRepository coffeeGameRepository;

    public void setCoffeeGame(Member member, CoffeeGameCreateRequest request){
        CoffeeGame addData = new CoffeeGame();
        addData.setMember(member);
        addData.setTotalPrice(request.getTotalPrice());
        addData.setOrderDate(LocalDate.now());

        coffeeGameRepository.save(addData);
    }

    public List<CoffeeGameItem> getCoffeeGames(){
        List<CoffeeGame> originList = coffeeGameRepository.findAll();

        List<CoffeeGameItem> result = new LinkedList<>();

        for (CoffeeGame coffeeGame : originList){
            CoffeeGameItem addItem = new CoffeeGameItem();
            addItem.setMemberId(coffeeGame.getMember().getId());
            addItem.setTotalPrice(coffeeGame.getTotalPrice());
            addItem.setOrderDate(coffeeGame.getOrderDate());

            result.add(addItem);
        }
        return result;
    }

    public CoffeeGameResponse getCoffeeGame(long id){
        CoffeeGame originData = coffeeGameRepository.findById(id).orElseThrow();
        CoffeeGameResponse response= new CoffeeGameResponse();
        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setTotalPrice(originData.getTotalPrice());
        response.setOrderDate(originData.getOrderDate());

        return response;
    }

    public void putCoffeeGame(long id, CoffeeGameTotalPriceChangeRequest request){
        CoffeeGame originData = coffeeGameRepository.findById(id).orElseThrow();
        originData.setTotalPrice(request.getTotalPrice());
        coffeeGameRepository.save(originData);
    }

    public void delCoffeeGame(long id){
        coffeeGameRepository.deleteById(id);
    }
}
