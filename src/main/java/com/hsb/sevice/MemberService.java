package com.hsb.sevice;

import com.hsb.entity.Member;
import com.hsb.model.member.MemberInfoChangeRequest;
import com.hsb.model.member.MemberItem;
import com.hsb.model.member.MemberCreateRequest;
import com.hsb.model.member.MemberResponse;
import com.hsb.reposutory.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setEtcMemo(member.getEtcMemo());

            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putMember(long id, MemberInfoChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setName(request.getName());
        originData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(originData);
    }
}
