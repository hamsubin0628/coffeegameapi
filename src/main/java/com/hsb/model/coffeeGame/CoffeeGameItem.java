package com.hsb.model.coffeeGame;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CoffeeGameItem {
    private Long memberId;
    private Double totalPrice;
    private LocalDate orderDate;
}
