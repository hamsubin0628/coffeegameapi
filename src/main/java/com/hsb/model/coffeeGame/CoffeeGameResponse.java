package com.hsb.model.coffeeGame;

import com.hsb.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CoffeeGameResponse {
    private Long memberId;
    private String memberName;
    private Double totalPrice;
    private LocalDate orderDate;
}
