package com.hsb.model.coffeeGame;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CoffeeGameTotalPriceChangeRequest {
    private Double totalPrice;
}
