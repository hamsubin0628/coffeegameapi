package com.hsb.model.coffeeGame;

import com.hsb.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CoffeeGameCreateRequest {
    private Member member;
    private Double totalPrice;
    private LocalDate orderDate;
}
