package com.hsb.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String name;
    private String phoneNumber;
    private String etcMemo;
}
