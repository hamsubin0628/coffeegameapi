package com.hsb.controller;

import com.hsb.entity.CoffeeGame;
import com.hsb.entity.Member;
import com.hsb.model.coffeeGame.CoffeeGameItem;
import com.hsb.model.coffeeGame.CoffeeGameCreateRequest;
import com.hsb.model.coffeeGame.CoffeeGameResponse;
import com.hsb.model.coffeeGame.CoffeeGameTotalPriceChangeRequest;
import com.hsb.sevice.CoffeeGameService;
import com.hsb.sevice.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/coffee-game")
public class CoffeeGameController {
    private final MemberService memberService;
    private final CoffeeGameService coffeeGameService;

    @PostMapping("/hell/member-id/{memberId}")
    public String setCoffeeGame(@PathVariable long memberId, @RequestBody CoffeeGameCreateRequest request){
        Member member = memberService.getData(memberId);
        coffeeGameService.setCoffeeGame(member, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<CoffeeGameItem> getCoffeeGames(){
        return coffeeGameService.getCoffeeGames();
    }

    @GetMapping("/detail/{id}")
    public CoffeeGameResponse getCoffeeGame(@PathVariable long id){
        return coffeeGameService.getCoffeeGame(id);
    }

    @PutMapping("/coffee-game-price/{id}")
    public String putCoffeeGame(@PathVariable long id, @RequestBody CoffeeGameTotalPriceChangeRequest request){
        coffeeGameService.putCoffeeGame(id, request);

        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delCoffeeGame(@PathVariable long id){
        coffeeGameService.delCoffeeGame(id);

        return "OK";
    }
}
