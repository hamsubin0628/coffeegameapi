package com.hsb.controller;

import com.hsb.model.member.MemberInfoChangeRequest;
import com.hsb.model.member.MemberItem;
import com.hsb.model.member.MemberCreateRequest;
import com.hsb.model.member.MemberResponse;
import com.hsb.sevice.MemberService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/new")
    public String setMember(@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers(){
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id){
        return memberService.getMember(id);
    }

    @PutMapping("/member-info-change.{id}")
    public String putMember(@PathVariable long id, @RequestBody MemberInfoChangeRequest request){
        memberService.putMember(id, request);

        return "OK";
    }
}
