package com.hsb.reposutory;

import com.hsb.entity.CoffeeGame;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoffeeGameRepository extends JpaRepository<CoffeeGame, Long> {
}
